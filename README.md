# openstreetmap

На странице на всю ширину и выстоту страницы нарисовать карту OpenStreetMap через библиотеку https://github.com/Leaflet/Leaflet

1. Построить линию на основе данных файла data.json из той же папки с координатами массива data[0] -> activeTrip -> data -> coordinates
2. Отрисовать маркер на координате data[0] -> activeTrip -> finishPosition
3. При нажатии на него поверх карты должен показаться html элемент:
 - 300 пикселей шириной
 - позиционирован в верхнем правом углу
 - с отступом от края экрана в 20 пикселей сверху и справа
 - текстом:
    "Название: "+ data[0] -> serial 
    "Дата остановки: "+ data[0] ->activeTrip -> finishPosition -> time

## Project setup
```
npm install
npm run serve
```