import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import L from "leaflet";
import "leaflet/dist/leaflet.css";

Vue.config.productionTip = false;
Vue.use(L);

new Vue({
  store,
  render: (h) => h(App),
}).$mount("#app");
